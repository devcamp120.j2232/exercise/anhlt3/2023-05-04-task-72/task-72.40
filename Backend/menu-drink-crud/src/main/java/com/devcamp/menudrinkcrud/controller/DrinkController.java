package com.devcamp.menudrinkcrud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menudrinkcrud.model.Drink;
import com.devcamp.menudrinkcrud.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrinks() {
        try {
            List<Drink> pDrink = new ArrayList<Drink>();
            pDrinkRepository.findAll().forEach(pDrink::add);
            return new ResponseEntity<>(pDrink, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") long id) {
        try {
            Optional<Drink> drinkData = pDrinkRepository.findById(id);
            if (drinkData.isPresent()) {
                return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Drink> createDrink(@Valid @RequestBody Drink pDrinks) {
        try {
            pDrinks.setNgayTao(new Date());
            pDrinks.setNgayCapNhat(null);
            Drink _drinks = pDrinkRepository.save(pDrinks);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Drink> updateDrink(@PathVariable("id") long id, @Valid @RequestBody Drink pDrinks) {
        try {
            Optional<Drink> drinkData = pDrinkRepository.findById(id);
            if (drinkData.isPresent()) {
                Drink drink = drinkData.get();
                drink.setMaNuocUong(pDrinks.getMaNuocUong());
                drink.setTenNuocUong(pDrinks.getTenNuocUong());
                drink.setDonGia(pDrinks.getDonGia());
                drink.setGhiChu(pDrinks.getGhiChu());
                drink.setNgayCapNhat(new Date());
                return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<Drink> deleteDrink(@PathVariable("id") long id) {
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks")
    public ResponseEntity<HttpStatus> deleteAllDrinks() {
        try {
            pDrinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
