package com.devcamp.menudrinkcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenuDrinkCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenuDrinkCrudApplication.class, args);
	}

}
