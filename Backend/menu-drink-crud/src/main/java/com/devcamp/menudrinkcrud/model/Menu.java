package com.devcamp.menudrinkcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "size")
    private char size;

    @Column(name = "duong_kinh")
    private int diameter;

    @Column(name = "suon")
    private int meat;

    @Column(name = "salad")
    private int salad;

    @Column(name = "so_luong_nuoc_ngot")
    private int drinkQuantity;

    @Column(name = "don_gia")
    private int price;

    public Menu() {
    }

    public Menu(long id, char size, int diameter, int meat, int salad, int drinkQuantity, int price) {
        this.id = id;
        this.size = size;
        this.diameter = diameter;
        this.meat = meat;
        this.salad = salad;
        this.drinkQuantity = drinkQuantity;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getMeat() {
        return meat;
    }

    public void setMeat(int meat) {
        this.meat = meat;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getDrinkQuantity() {
        return drinkQuantity;
    }

    public void setDrinkQuantity(int drinkQuantity) {
        this.drinkQuantity = drinkQuantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
