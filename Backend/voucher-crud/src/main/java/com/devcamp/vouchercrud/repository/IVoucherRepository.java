package com.devcamp.vouchercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.vouchercrud.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
