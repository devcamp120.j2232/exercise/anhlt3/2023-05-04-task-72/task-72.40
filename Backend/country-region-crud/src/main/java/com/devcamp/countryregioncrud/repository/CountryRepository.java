package com.devcamp.countryregioncrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.countryregioncrud.model.CCountry;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);

	CCountry findByCountryCodeContaining(String countryCode);

	CCountry findByCountryName(String countryName);
}
