package com.devcamp.countryregioncrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.countryregioncrud.model.CCountry;
import com.devcamp.countryregioncrud.model.CRegion;
import com.devcamp.countryregioncrud.repository.CountryRepository;
import com.devcamp.countryregioncrud.repository.RegionRepository;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/countries/{countryid}/regions")
	public ResponseEntity<Object> createRegion(@PathVariable("countryid") Long countryId, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(countryId);
			if (countryData.isPresent()) {
				CRegion newRegion = new CRegion();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				// newRegion.setCountry(cRegion.getCountry());

				CCountry _country = countryData.get();
				newRegion.setCountry(_country);
				newRegion.setCountryName(_country.getCountryName());

				CRegion savedRole = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Region: " + e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/regions/{regionid}")
	public ResponseEntity<Object> updateRegion(@PathVariable("regionid") Long regionId, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(regionId);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions/{regionid}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable("regionid") Long regionId) {
		try {
			regionRepository.deleteById(regionId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions")
	public ResponseEntity<Object> deleteAllRegions() {
		try {
			regionRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions/{regionid}")
	public CRegion getRegionById(@PathVariable("regionid") Long regionId) {
		if (regionRepository.findById(regionId).isPresent())
			return regionRepository.findById(regionId).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/regions")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions")
	public List<CRegion> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}

	@CrossOrigin
	@GetMapping("/country1/{countryId}/regions1/{id}")
	public Optional<CRegion> getRegionByRegionCAndCountryC(@PathVariable(value = "countryId") String countryId,
			@PathVariable(value = "id") String regionId) {
		return regionRepository.findByRegionCodeAndCountryCountryCode(regionId, countryId);
	}

	@CrossOrigin
	@GetMapping("/regions-count/{countryId}")
	public Long countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.countByCountryId(countryId);
	}

	@CrossOrigin
	@GetMapping("/regions/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}

	@CrossOrigin
	@GetMapping("/regions/code/{code}")
	public CRegion getRegionByCode(@PathVariable String code) {
		return regionRepository.findByRegionCode(code);
	}

	@CrossOrigin
	@GetMapping("/regions/name/{name}")
	public CRegion getRegionByName(@PathVariable String name) {
		return regionRepository.findByRegionName(name);
	}
}
