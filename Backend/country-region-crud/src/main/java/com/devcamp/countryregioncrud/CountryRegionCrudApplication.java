package com.devcamp.countryregioncrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryRegionCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryRegionCrudApplication.class, args);
	}

}
