package com.devcamp.countryregioncrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.countryregioncrud.model.CCountry;
import com.devcamp.countryregioncrud.repository.CountryRepository;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry country = new CCountry();
			country.setCountryName(cCountry.getCountryName());
			country.setCountryCode(cCountry.getCountryCode());
			country.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(country);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Country: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/countries/{countryid}")
	public ResponseEntity<Object> updateCountry(@PathVariable("countryid") Long countryId, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(countryId);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Viết method delete a country: deleteCountryById() sử dụng hàm deleteById()
	 * của CountryRepository
	 * 
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@DeleteMapping("/countries/{countryid}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable("countryid") Long countryId) {
		try {
			Optional<CCountry> optional = countryRepository.findById(countryId);
			if (optional.isPresent()) {
				countryRepository.deleteById(countryId);
			} else {
				// countryRepository.deleteById(id);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/countries")
	public ResponseEntity<Object> deleteAllCountries() {
		try {
			countryRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@CrossOrigin
	@GetMapping("/countries/{countryid}")
	public CCountry getCountryById(@PathVariable("countryid") Long countryId) {
		if (countryRepository.findById(countryId).isPresent())
			return countryRepository.findById(countryId).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/countries")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/countries/code/{code}")
	public CCountry getCountryByCode(@PathVariable String code) {
		return countryRepository.findByCountryCode(code);
	}

	@CrossOrigin
	@GetMapping("/countries/name/{name}")
	public CCountry getCountryByName(@PathVariable String name) {
		return countryRepository.findByCountryName(name);
	}

	/**
	 * Viết method đếm country có trong CSDL: countCountry() sử
	 * dụng hàm count() của CountryRepository
	 * 
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/countries-count")
	public long countCountry() {
		return countryRepository.count();
	}

	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử
	 * dụng hàm existsById() của CountryRepository
	 * 
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/countries/check/{countryid}")
	public boolean checkCountryById(@PathVariable("countryid") Long countryId) {
		return countryRepository.existsById(countryId);
	}

	/**
	 * Tìm country có chứa giá trị trong country code
	 * 
	 * @param code
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/countries/containing-code/{code}")
	public CCountry getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}
}
