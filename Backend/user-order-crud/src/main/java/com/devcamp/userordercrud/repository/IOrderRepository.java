package com.devcamp.userordercrud.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.userordercrud.model.COrder;

public interface IOrderRepository extends CrudRepository<COrder, Long> {
    List<COrder> findBycUserId(Long id);
}
