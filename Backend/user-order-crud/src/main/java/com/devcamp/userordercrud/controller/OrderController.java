package com.devcamp.userordercrud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userordercrud.model.COrder;
import com.devcamp.userordercrud.model.CUser;
import com.devcamp.userordercrud.repository.IOrderRepository;
import com.devcamp.userordercrud.repository.IUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class OrderController {
    @Autowired
    IUserRepository pUserRepository;

    @Autowired
    IOrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        List<COrder> orderList = new ArrayList<COrder>();
        orderRepository.findAll().forEach(orderElement -> {
            orderList.add(orderElement);
        });

        if (!orderList.isEmpty()) {
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{userId}/orders")
    public ResponseEntity<Object> getOrderByUserId(@PathVariable(name = "userId") Long userId) {
        List<COrder> orderList = orderRepository.findBycUserId(userId);
        if (orderList != null && !orderList.isEmpty()) {
            return ResponseEntity.ok(orderList);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(name = "id") Long id) {
        Optional<COrder> _order = orderRepository.findById(id);
        if (_order.isPresent()) {
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users/{userid}/orders")
    public ResponseEntity<Object> createOrder(@PathVariable(name = "userid") Long userId,
            @RequestBody COrder newOrder) {
        Optional<CUser> _user = pUserRepository.findById(userId);
        if (!_user.isPresent()) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

        COrder _order = new COrder();
        try {
            _order.setCreated(new Date());
            _order.setUpdated(null);
            _order.setOrderCode(newOrder.getOrderCode());
            _order.setVoucherCode(newOrder.getVoucherCode());
            _order.setPizzaSize(newOrder.getPizzaSize());
            _order.setPizzaType(newOrder.getPizzaType());
            _order.setPrice(newOrder.getPrice());
            _order.setPaid(newOrder.getPaid());
            _order.setcUser(_user.get());
            orderRepository.save(_order);
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Fail to Create specified order: " + e.getCause().getMessage());
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> _orderData = orderRepository.findById(id);
        if (_orderData.isPresent()) {
            COrder _order = _orderData.get();
            _order.setUpdated(new Date());
            _order.setPizzaSize(orderUpdate.getPizzaSize());
            _order.setPizzaType(orderUpdate.getPizzaType());
            _order.setPrice(orderUpdate.getPrice());
            _order.setPaid(orderUpdate.getPaid());
            _order.setVoucherCode(orderUpdate.getVoucherCode());
            try {
                return ResponseEntity.ok(orderRepository.save(_order));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not excute operation of this Entity: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable("id") Long id) {
        try {
            orderRepository.deleteById(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Can not excute operation of this Entity: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/orders")
    public ResponseEntity<Object> deleteOrder() {
        try {
            orderRepository.deleteAll();
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Can not excute operation of this Entity: " + e.getCause().getCause().getMessage());
        }
    }
}
